<?php


namespace Pondit\calculator\NumberCalculator;


class Addition
{
    public $number1 = null;
    public $number2 = null;

    public function __construct($n1,$n2)
    {
        $this->number1=$n1;
        $this->number2=$n2;
    }
}