<?php

include_once 'vendor/autoload.php';

use Pondit\calculator\NumberCalculator\Addition;
use Pondit\calculator\NumberCalculator\Substraction;
use Pondit\calculator\NumberCalculator\Multiplication;
use Pondit\calculator\NumberCalculator\Division;

$addition1=new Addition(2,3);
$addition2=new Addition(5,4);
var_dump($addition1);
var_dump($addition2);

$substraction1 = new Substraction();
var_dump($substraction1);

$multiplication1 = new Multiplication();
var_dump($multiplication1);

$division1 = new Division();
var_dump($division1);